var calculator = {
    printerId : "",
    arithmetic : /(\d+\.\d+|\d+)\s?([\+\*/-])\s?(\d+\.\d+|\d+)/,

    parse : function (expression) {
        var result = 0;
        var match = expression.match(this.arithmetic);
        if(match){
            switch (match[2]){
                case '+':
                    result = this.sum(match[1], match[3]);
                    break;
                case '-':
                    result = this.subtraction(match[1], match[3]);
                    break;
                case '*':
                    result = this.multiply(match[1], match[3]);
                    break;
                case '/':
                    result = this.division(match[1], match[3]);
                    break;
                default:
                    break;
            }

            this.isOk();
            return result;
        }

        this.isError();
        return "Invalid expression";
    },

    isOk: function () {
        $('#'+this.printerId)
            .addClass("alert-success")
            .removeClass("alert-warning");
    },

    isError: function () {
        $('#'+this.printerId)
            .addClass("alert-warning")
            .removeClass("alert-success");
    },

    print: function (expression) {
        var result = this.parse(expression);
        $('#'+this.printerId).html("Result: " + result);
    },

    sum: function (operatorOne, operatorTwo) {
        return parseFloat(operatorOne) + parseFloat(operatorTwo);
    },

    subtraction: function (operatorOne, operatorTwo) {
        return parseFloat(operatorOne) - parseFloat(operatorTwo);
    },

    multiply: function (operatorOne, operatorTwo) {
        return parseFloat(operatorOne) * parseFloat(operatorTwo);
    },

    division: function (operatorOne, operatorTwo) {
        return parseFloat(operatorOne) / parseFloat(operatorTwo);
    }
};